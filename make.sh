#!/bin/bash

type inkscape &>/dev/null || { echo '"inkscape" command not found'; exit 1; }

cmdfile="$(mktemp)"
trap 'rm "$cmdfile"' EXIT

for section in "scalable"/*; do
    for size in 16 18 22 24 32 48 64; do
        mkdir -p "$size/$(basename "$section")"
    done

    for file in "$section"/*; do
        printf 'file-open:%s;\n' "$file" >>"$cmdfile"
        outfile="$size/$(basename "$section")/$(basename "${file%.*}.png")"

        for size in 16 18 22 24 32 48 64; do
            if [[ -L "$file" ]]; then
                orig="$(readlink "$file")"
                target="${orig%.*}.png"

                ln -sf "$target" "$outfile"
            else
                outfile="$size/$(basename "$section")/$(basename "${file%.*}.png")"
                printf 'export-width:%s; export-filename:%s; export-do;\n' "$size" "$outfile" >>"$cmdfile"
            fi
        done
    done
done

inkscape --shell <"$cmdfile" >/dev/null

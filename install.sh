#!/bin/bash

printf '=> copy icon theme directory...\n'
sudo cp -r . /usr/share/icons/NumixCircleDhg

printf '=> update GTK icon cache...\n'
sudo gtk-update-icon-cache '/usr/share/icons/NumixCircleDhg' -f -y
